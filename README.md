
https://www.youtube.com/watch?v=0NT8KRXRFG8
https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/ses-examples-sending-email.html


This is purely a command line option at the moment.

1. Connect your AWS account with aws configure, must be an account allowed to send emails SES

2. Set these vars.

** NOTE ** arccorp.com accounts dont seem to work (probably security issues, I have been testing to my gmail account).

TIP_EMAIL_FROM=your from email
TIP_EMAIL_TO=your to email
TIP_EMAIL_TEMPLATE_NAME=default template name

e.g.

TIP_EMAIL_FROM=sbotham1968@gmail.com
TIP_EMAIL_TO=sbotham1968@gmail.com
TIP_EMAIL_TEMPLATE_NAME=results

run

3. node templatedEmail {templateName} {sendEmail}      (If the named template doesnt exist you will get an error)

e.g.

node templatedEmail results true

{templateName} 

    existing templates:-

       remaining (The remaing searches based on your TIP subscription)
       noresults (The no results email sent to the customer)
       results (The results email sent to the customer)

{sendEmail}

    true or false

    if true actually sends the email
    if false does everything but physically sending the email (for testing and to keep our costs down)



Templates can be either be setup manually via the aws cli using a json file as input template or terraform apply <- to do

AWS SES CLI:- (From inside emailTemplates folder)

**** CREATE ****

aws ses create-template --cli-input-json file://remaining.json
aws ses create-template --cli-input-json file://results.json
aws ses create-template --cli-input-json file://noresults.json


**** DELETE ****

aws ses delete-template --template-name remaining

**** UPDATE ****

aws ses update-template --cli-input-json file://remaining.json
aws ses update-template --cli-input-json file://results.json
aws ses update-template --cli-input-json file://noresults.json


existing templates on AWS can be listed at 

https://console.aws.amazon.com/ses/home?region=us-east-1#email-templates-list:





Tools to validate the build templateData (If this is incorrect the email will show as send, but wont be).

https://jsonlint.com/

https://www.freeformatter.com/json-escape.html




see /terraform here we have examples of how to create templates using terraform





if you run into this issue... when running yarn install

[1/4] Resolving packages...
error An unexpected error occurred: "https://registry.yarnpkg.com/@tipsters%2fcommon-utils: Not found".


npm-cli-login -u svcuser -p ********* -e your_email@arccorp.com -r https://nexus.arc.travel/repository/npm-private/ -s @tipsters --config-path ~/.npmrc


Transfer repo from user (sbotham) to Webapps org

   https://help.github.com/en/articles/transferring-a-repository


